#ifndef __ATOM_BUILTINS__
#define __ATOM_BUILTINS__

#define CAS(ptr,old,n) __sync_bool_compare_and_swap(ptr,old,n)
#define VCAS(ptr,old,n) __sync_val_compare_and_swap(ptr,old,n)

#define atom_add(...) __sync_fetch_and_add(__VA_ARGS__)
#define atom_sub(...) __sync_fetch_and_sub(__VA_ARGS__)
#define atom_or(...) __sync_fetch_and_or(__VA_ARGS__)
#define atom_and(...) __sync_fetch_and_and(__VA_ARGS__)	
#define atom_xor(...) __sync_fetch_and_xor(__VA_ARGS__)
#define atom_nand(...) __sync_fetch_and_nand(__VA_ARGS__)

#define add_atom(...) __sync_add_and_fetch(__VA_ARGS__)
#define sub_atom(...) __sync_sub_and_fetch(__VA_ARGS__)
#define or_atom(...) __sync_or_and_fetch(__VA_ARGS__)
#define and_atom(...) __sync_and_and_fetch(__VA_ARGS__)	
#define xor_atom(...) __sync_xor_and_fetch(__VA_ARGS__)
#define nand_atom(...) __sync_nand_and_fetch(__VA_ARGS__)

#define barrier() __sync_synchronize()

#endif
